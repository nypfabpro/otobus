#-------------------------------------------------
#
# Project created by QtCreator 2015-04-20T13:48:34
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = belediyeTasimaciligiYonetimi
TEMPLATE = app


SOURCES += main.cpp\
        belediyetasimaciligi.cpp \
    veriler/veritabani.cpp \
    formlar/otobushakkinda.cpp \
    formlar/otobushatlar.cpp \
    formlar/otobusduraklar.cpp \
    formlar/otobushatekle.cpp \
    formlar/otobusdurakekle.cpp \
    formlar/gunlukverikaydi.cpp \
    formlar/otobusdurakduzenle.cpp \
    formlar/otobushatduzenle.cpp \
    veriler/belediyesinifi.cpp \
    formlar/veritutulanyer.cpp \
    formlar/grafeklentisi.cpp

HEADERS  += belediyetasimaciligi.h \
    veriler/veritabani.h \
    formlar/otobushakkinda.h \
    formlar/otobushatlar.h \
    formlar/otobusduraklar.h \
    formlar/otobushatekle.h \
    formlar/otobusdurakekle.h \
    formlar/gunlukverikaydi.h \
    formlar/otobusdurakduzenle.h \
    formlar/otobushatduzenle.h \
    veriler/belediyesinifi.h \
    formlar/veritutulanyer.h \
    formlar/grafeklentisi.h

FORMS    += belediyetasimaciligi.ui \
    formlar/otobushakkinda.ui \
    formlar/otobushatlar.ui \
    formlar/otobusduraklar.ui \
    formlar/otobushatekle.ui \
    formlar/otobusdurakekle.ui \ 
    formlar/gunlukverikaydi.ui \
    formlar/otobusdurakduzenle.ui \
    formlar/otobushatduzenle.ui \
    formlar/veritutulanyer.ui \
    formlar/grafeklentisi.ui

RESOURCES +=
