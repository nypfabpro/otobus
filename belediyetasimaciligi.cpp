#include "belediyetasimaciligi.h"
#include "ui_belediyetasimaciligi.h"
#include <QCloseEvent>

belediyeTasimaciligi::belediyeTasimaciligi(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::belediyeTasimaciligi)
{
    ui->setupUi(this);

    su_ptrVeritabani = new veritabani();
    su_ptrVeritabani->bilgileriOku();
}

belediyeTasimaciligi::~belediyeTasimaciligi()
{
    delete ui;
}



void belediyeTasimaciligi::on_actionOtobusHakkinda_triggered()
{
    otobusHakkinda form;
    form.setVeriTabani(su_ptrVeritabani);
    form.exec();
}



void belediyeTasimaciligi::on_actionOtobusHatlar_triggered()
{
    otobusHatlar form;
    form.setVeriTabani(su_ptrVeritabani);
    form.exec();
}

void belediyeTasimaciligi::on_actionOtobusDuraklar_triggered()
{
    otobusDuraklar form;
    form.setVeriTabani(su_ptrVeritabani);
    form.exec();
}

void belediyeTasimaciligi::on_actionOtobusHatEkle_triggered()
{
    otobusHatEkle form;
    form.setVeriTabani(su_ptrVeritabani);
    ptrOtobusHatSinifi yeniOtobusHat = new OtobusHatSinifi;
    form.setOtobusHat(yeniOtobusHat);
    form.exec();
}

void belediyeTasimaciligi::on_actionOtobusDurakEkle_triggered()
{
    otobusDurakEkle form;
    form.setVeriTabani(su_ptrVeritabani);
    ptrOtobusDurakSinifi yeniOtobusDurak = new OtobusDurakSinifi;
    form.setOtobusDurak(yeniOtobusDurak);
    form.exec();
}



void belediyeTasimaciligi::on_actionGunlukVeriKaydi_triggered()
{
    gunlukVeriKaydi form;
    form.setVeriTabani(su_ptrVeritabani);
    form.exec();
}

void belediyeTasimaciligi::closeEvent(QCloseEvent *event)
{
    su_ptrVeritabani->bilgileriKaydet();
}

void belediyeTasimaciligi::on_actionGirilecek_Bilgiler_triggered()
{
    grafeklentisi form;
    form.setVeriTabani(su_ptrVeritabani);
    form.exec();
}

void belediyeTasimaciligi::on_actionVeri_Tutulacak_Tablo_triggered()
{
    VeriTutulanYer form;
    form.setVeriTabani(su_ptrVeritabani);
    form.exec();
}
