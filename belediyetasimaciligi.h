#ifndef BELEDIYETASIMACILIGI_H
#define BELEDIYETASIMACILIGI_H

#include <QMainWindow>
#include <QMessageBox>
#include <QLineEdit>


#include "formlar/gunlukverikaydi.h"

#include "formlar/otobusdurakekle.h"
#include "formlar/otobusdurakduzenle.h"
#include "formlar/otobusduraklar.h"
#include "formlar/otobushakkinda.h"
#include "formlar/otobushatekle.h"
#include "formlar/otobushatduzenle.h"
#include "formlar/otobushatlar.h"

#include "formlar/veritutulanyer.h"
#include "formlar/grafeklentisi.h"
#include "veriler/veritabani.h"


namespace Ui {
class belediyeTasimaciligi;
}

class belediyeTasimaciligi : public QMainWindow
{
    Q_OBJECT

public:
    explicit belediyeTasimaciligi(QWidget *parent = 0);
    ~belediyeTasimaciligi();

private slots:


    void on_actionOtobusHakkinda_triggered();



    void on_actionOtobusHatlar_triggered();

    void on_actionOtobusDuraklar_triggered();

    void on_actionOtobusHatEkle_triggered();

    void on_actionOtobusDurakEkle_triggered();


    void on_actionGunlukVeriKaydi_triggered();

    void on_actionGirilecek_Bilgiler_triggered();

    void closeEvent(QCloseEvent *event);

    void on_actionVeri_Tutulacak_Tablo_triggered();

private:
    Ui::belediyeTasimaciligi *ui;

    veritabani *su_ptrVeritabani;
};

#endif // BELEDIYETASIMACILIGI_H
