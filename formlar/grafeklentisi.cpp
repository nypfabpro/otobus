#include "grafeklentisi.h"
#include "ui_grafeklentisi.h"

grafeklentisi::grafeklentisi(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::grafeklentisi)
{
    ui->setupUi(this);
}

grafeklentisi::~grafeklentisi()
{
    delete ui;
}

void grafeklentisi::setVeriTabani(veritabani *vt)
{
    if(su_ptrVeriTabani != vt) {
        su_ptrVeriTabani = vt;
    }
}

void grafeklentisi::setGrafEklenti(ptrGrafEklenti graf)
{
    if(su_ptrGrafEklenti != graf) {
        su_ptrGrafEklenti = graf;
    }
}
void grafeklentisi::on_btnHesaplaOtobus_clicked()
{

}

void grafeklentisi::on_btnHesaplaMetrobus_clicked()
{

}

void grafeklentisi::on_btnHesaplaTramvay_clicked()
{

}

void grafeklentisi::on_btnHesaplaMetro_clicked()
{

}

void grafeklentisi::on_btnHesaplaDeniz_clicked()
{

}
