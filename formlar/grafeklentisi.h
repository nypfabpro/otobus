#ifndef GRAFEKLENTISI_H
#define GRAFEKLENTISI_H

#include <QDialog>
#include <QMessageBox>
#include "veriler/veritabani.h"

namespace Ui {
class grafeklentisi;
}

class grafeklentisi : public QDialog
{
    Q_OBJECT

public:
    explicit grafeklentisi(QDialog *parent = 0);
    ~grafeklentisi();

    void setVeriTabani(veritabani *vt);
    void setGrafEklenti(ptrGrafEklenti graf);

private slots:
    void on_btnHesaplaOtobus_clicked();

    void on_btnHesaplaMetrobus_clicked();

    void on_btnHesaplaTramvay_clicked();

    void on_btnHesaplaMetro_clicked();

    void on_btnHesaplaDeniz_clicked();

private:
    Ui::grafeklentisi *ui;

    veritabani *su_ptrVeriTabani;
    ptrGrafEklenti su_ptrGrafEklenti;
};

#endif // GRAFEKLENTISI_H
