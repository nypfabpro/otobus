#include "gunlukverikaydi.h"
#include "ui_gunlukverikaydi.h"

gunlukVeriKaydi::gunlukVeriKaydi(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::gunlukVeriKaydi)
{
    ui->setupUi(this);
}

gunlukVeriKaydi::~gunlukVeriKaydi()
{
    delete ui;
}

void gunlukVeriKaydi::setVeriTabani(veritabani *vt)
{
    if(su_ptrVeriTabani != vt) {
        su_ptrVeriTabani = vt;
    }
}

void gunlukVeriKaydi::setVeriKaydi(ptrVeriKayit veri)
{
    if(su_ptrVeriKaydi != veri) {
        su_ptrVeriKaydi = veri;

        ui->lineOtobus->setText(QString::number((su_ptrVeriKaydi->OtobusGelen())));
        ui->lineOtobus->setText("");


    }
}

void gunlukVeriKaydi::on_btnKaydet_clicked()
{
    su_ptrVeriKaydi->setOtobusGelen(ui->lineOtobus->text().toDouble());

    su_ptrVeriTabani->veriKayitEkle(su_ptrVeriKaydi);

    setVeriKaydi(new veriKayitSinifi);
}

void gunlukVeriKaydi::on_btnKapat_clicked()
{
    reject();
}
