#ifndef GUNLUKVERIKAYDI_H
#define GUNLUKVERIKAYDI_H

#include <QDialog>
#include "veriler/veritabani.h"

namespace Ui {
class gunlukVeriKaydi;
}

class gunlukVeriKaydi : public QDialog
{
    Q_OBJECT

public:
    explicit gunlukVeriKaydi(QDialog *parent = 0);
    ~gunlukVeriKaydi();

    void setVeriTabani(veritabani *vt);
    void setVeriKaydi(ptrVeriKayit veri);

private slots:
    void on_btnKaydet_clicked();

    void on_btnKapat_clicked();

private:
    Ui::gunlukVeriKaydi *ui;

    veritabani *su_ptrVeriTabani;
    ptrVeriKayit su_ptrVeriKaydi;
};

#endif // GUNLUKVERIKAYDI_H
