#include "otobusdurakduzenle.h"
#include "ui_otobusdurakduzenle.h"

otobusDurakDuzenle::otobusDurakDuzenle(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::otobusDurakDuzenle)
{
    ui->setupUi(this);
    ui->lineDurakKodu->setEnabled(0);
}

otobusDurakDuzenle::~otobusDurakDuzenle()
{
    delete ui;
}

void otobusDurakDuzenle::setVeriTabani(veritabani *vt)
{
    if(su_ptrVeriTabani != vt) {
        su_ptrVeriTabani = vt;
    }
}

void otobusDurakDuzenle::setOtobusDurakDuzenle(ptrOtobusDurakSinifi durak)
{
    if(su_ptrOtobusDurak != durak) {
        su_ptrOtobusDurak = durak;

        ui->lineDurakAdi->setText(su_ptrOtobusDurak->DurakAdi());
        //ui->lineDurakKodu->setText(su_ptrOtobusDurak->DurakKodu());
  }
}

void otobusDurakDuzenle::on_btnKaydet_clicked()
{
    if(ui->lineDurakAdi->text()=="") {
        QMessageBox::critical(this, "Eksik Giriş","Lütfen Gerekli Yerleri Doldurunuz.");
    } else {
        su_ptrOtobusDurak->setDurakAdi(ui->lineDurakAdi->text());
        //su_ptrOtobusDurak->setDurakKodu(ui->lineDurakKodu->text());

          ui->lineDurakKodu->setFocus();
}
}
void otobusDurakDuzenle::on_btnKapat_clicked()
{
    reject();
}
