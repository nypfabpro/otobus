#ifndef OTOBUSDURAKDUZENLE_H
#define OTOBUSDURAKDUZENLE_H

#include <QDialog>
#include <QMessageBox>
#include "veriler/veritabani.h"

namespace Ui {
class otobusDurakDuzenle;
}

class otobusDurakDuzenle : public QDialog
{
    Q_OBJECT

public:
    explicit otobusDurakDuzenle(QDialog *parent = 0);
    ~otobusDurakDuzenle();

    void setVeriTabani(veritabani *vt);
    void setOtobusDurakDuzenle(ptrOtobusDurakSinifi durak);

private slots:
    void on_btnKaydet_clicked();

    void on_btnKapat_clicked();

private:
    Ui::otobusDurakDuzenle *ui;

    veritabani *su_ptrVeriTabani;
    ptrOtobusDurakSinifi su_ptrOtobusDurak;
};

#endif // OTOBUSDURAKDUZENLE_H
