#include "otobusdurakekle.h"
#include "ui_otobusdurakekle.h"

otobusDurakEkle::otobusDurakEkle(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::otobusDurakEkle)
{
    ui->setupUi(this);
    ui->lineDurakKodu->setEnabled(0);
}

otobusDurakEkle::~otobusDurakEkle()
{
    delete ui;
}

void otobusDurakEkle::setVeriTabani(veritabani *vt)
{
    if(su_ptrVeriTabani != vt) {
        su_ptrVeriTabani = vt;
    }
}

void otobusDurakEkle::setOtobusDurak(ptrOtobusDurakSinifi durak)
{
    if(su_ptrOtobusDurak != durak){
        su_ptrOtobusDurak = durak;

        ui->lineDurakAdi->setText(su_ptrOtobusDurak->DurakAdi());
        ui->lineDurakKodu->setText(su_ptrOtobusDurak->DurakKodu());
        ui->lineDurakKodu->setText(QString::number(su_ptrVeriTabani->OtobusDurak().count()+1));
  }
}

void otobusDurakEkle::on_btnKaydet_clicked()
{
    if(ui->lineDurakAdi->text()=="" || ui->lineDurakKodu->text()=="") {
        QMessageBox::critical(this, "Eksik Giriş","Lütfen Gerekli Yerleri Doldurunuz.");
    } else {
        su_ptrOtobusDurak->setDurakAdi(ui->lineDurakAdi->text());
        su_ptrOtobusDurak->setDurakKodu(ui->lineDurakKodu->text());

        su_ptrVeriTabani->otobusDurakEkle(su_ptrOtobusDurak);

        setOtobusDurak(new OtobusDurakSinifi);
        ui->lineDurakKodu->setFocus();
  }
}

void otobusDurakEkle::on_btnKapat_clicked()
{
    reject();
}
