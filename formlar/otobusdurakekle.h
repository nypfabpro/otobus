#ifndef OTOBUSDURAKEKLE_H
#define OTOBUSDURAKEKLE_H

#include <QDialog>
#include <QMessageBox>
#include "veriler/veritabani.h"

namespace Ui {
class otobusDurakEkle;
}

class otobusDurakEkle : public QDialog
{
    Q_OBJECT

public:
    explicit otobusDurakEkle(QDialog *parent = 0);
    ~otobusDurakEkle();

    void setVeriTabani(veritabani *vt);
    void setOtobusDurak(ptrOtobusDurakSinifi durak);

private slots:
    void on_btnKaydet_clicked();

    void on_btnKapat_clicked();

private:
    Ui::otobusDurakEkle *ui;

    veritabani *su_ptrVeriTabani;
    ptrOtobusDurakSinifi su_ptrOtobusDurak;
};

#endif // OTOBUSDURAKEKLE_H
