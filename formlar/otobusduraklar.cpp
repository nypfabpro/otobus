#include "otobusduraklar.h"
#include "ui_otobusduraklar.h"

#include <QTableWidgetItem>
#include <QStringList>

otobusDuraklar::otobusDuraklar(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::otobusDuraklar)
{
    ui->setupUi(this);
}

otobusDuraklar::~otobusDuraklar()
{
    delete ui;
}

void otobusDuraklar::tabloGuncelle(OtobusDurakSinifiListesi liste)
{
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(liste.count());

    QStringList sutunBasliklari;
    sutunBasliklari.append("Durak Adı");
    sutunBasliklari.append("Durak Kodu");

    ui->tableWidget->setHorizontalHeaderLabels(sutunBasliklari);

    for(int i=0;i<liste.count();i++) {
        QTableWidgetItem *adHucresi = new QTableWidgetItem;
        adHucresi->setText(liste[i]->DurakAdi());
        ui->tableWidget->setItem(i,0,adHucresi);

        QTableWidgetItem *kodHucresi = new QTableWidgetItem;
        kodHucresi->setText(liste[i]->DurakKodu());
        ui->tableWidget->setItem(i,1,kodHucresi);
   }
}

void otobusDuraklar::setVeriTabani(veritabani *vt)
{
    if(su_ptrVeriTabani != vt) {
        su_ptrVeriTabani = vt;

        OtobusDurakSinifiListesi liste = su_ptrVeriTabani->OtobusDurak();
        tabloGuncelle(liste);
    }
}


void otobusDuraklar::on_pushButtonGuncelle_clicked()
{
    otobusDurakDuzenle form;
    form.setVeriTabani(su_ptrVeriTabani);
    form.exec();

    OtobusDurakSinifiListesi liste = su_ptrVeriTabani->OtobusDurak();
    tabloGuncelle(liste);
}
