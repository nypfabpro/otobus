#ifndef OTOBUSDURAKLAR_H
#define OTOBUSDURAKLAR_H

#include <QDialog>
#include "veriler/veritabani.h"
#include "otobusdurakduzenle.h"

namespace Ui {
class otobusDuraklar;
}

class otobusDuraklar : public QDialog
{
    Q_OBJECT

public:
    explicit otobusDuraklar(QDialog *parent = 0);
    ~otobusDuraklar();

    void setVeriTabani(veritabani *vt);

    void tabloGuncelle(OtobusDurakSinifiListesi liste);

private slots:
    void on_pushButtonGuncelle_clicked();

private:
    Ui::otobusDuraklar *ui;

    veritabani *su_ptrVeriTabani;
};

#endif // OTOBUSDURAKLAR_H
