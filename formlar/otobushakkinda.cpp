#include "otobushakkinda.h"
#include "ui_otobushakkinda.h"

otobusHakkinda::otobusHakkinda(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::otobusHakkinda)
{
    ui->setupUi(this);
}

otobusHakkinda::~otobusHakkinda()
{
    delete ui;
}

void otobusHakkinda::setVeriTabani(veritabani *vt)
{
    if(su_ptrVeriTabani != vt) {
        su_ptrVeriTabani = vt;
    }
}
