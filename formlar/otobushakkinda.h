#ifndef OTOBUSHAKKINDA_H
#define OTOBUSHAKKINDA_H

#include <QDialog>
#include "veriler/veritabani.h"

namespace Ui {
class otobusHakkinda;
}

class otobusHakkinda : public QDialog
{
    Q_OBJECT

public:
    explicit otobusHakkinda(QDialog *parent = 0);
    ~otobusHakkinda();

    void setVeriTabani(veritabani *vt);

private:
    Ui::otobusHakkinda *ui;

    veritabani *su_ptrVeriTabani;
};

#endif // OTOBUSHAKKINDA_H
