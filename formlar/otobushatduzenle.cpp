#include "otobushatduzenle.h"
#include "ui_otobushatduzenle.h"

otobusHatDuzenle::otobusHatDuzenle(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::otobusHatDuzenle)
{
    ui->setupUi(this);
    ui->lineAracKodu->setEnabled(0);
}

otobusHatDuzenle::~otobusHatDuzenle()
{
    delete ui;
}

void otobusHatDuzenle::setVeriTabani(veritabani *vt)
{
    if(su_ptrVeriTabani != vt) {
        su_ptrVeriTabani = vt;
    }
}

void otobusHatDuzenle::setOtobusHatDuzenle(ptrOtobusHatSinifi hat)
{
    if(su_ptrOtobusHat != hat) {
        su_ptrOtobusHat = hat;

        ekranaAktarma();

    }
}

void otobusHatDuzenle::ekranaAktarma()
{
    //ui->lineAracKodu->setText(su_ptrOtobusHat->AracKodu());
    ui->lineSoforAdiSoyadi->setText(su_ptrOtobusHat->SoforAdiSoyadi());
    ui->lineAyaktakiYolcuSayisi->setText(su_ptrOtobusHat->AyaktakiYolcuSayisi());
    ui->lineOturanYolcuSayisi->setText(su_ptrOtobusHat->OturanYolcuSayisi());
}

void otobusHatDuzenle::on_btnKaydet_clicked()
{
    if( //ui->lineAracKodu->text()== ""            ||
        ui->lineSoforAdiSoyadi->text()== ""     ||
        ui->lineAyaktakiYolcuSayisi->text()== ""    ||
        ui->lineOturanYolcuSayisi->text()== "") {
    QMessageBox::critical(this, "Eksik Giriş","Lütfen Gerekli Yerleri Doldurunuz.");
} else {
    //su_ptrOtobusHat->setAracKodu(ui->lineAracKodu->text());
    su_ptrOtobusHat->setSoforAdiSoyadi(ui->lineSoforAdiSoyadi->text());
    su_ptrOtobusHat->setAyaktakiYolcuSayisi(ui->lineAyaktakiYolcuSayisi->text());
    su_ptrOtobusHat->setOturanYolcuSayisi(ui->lineOturanYolcuSayisi->text());
      ui->lineAracKodu->setFocus();
  }
}

void otobusHatDuzenle::on_btnKapat_clicked()
{
    reject();
}
