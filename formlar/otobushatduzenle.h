#ifndef OTOBUSHATDUZENLE_H
#define OTOBUSHATDUZENLE_H

#include <QDialog>
#include <QMessageBox>
#include "veriler/veritabani.h"

namespace Ui {
class otobusHatDuzenle;
}

class otobusHatDuzenle : public QDialog
{
    Q_OBJECT

public:
    explicit otobusHatDuzenle(QDialog *parent = 0);
    ~otobusHatDuzenle();

    void setVeriTabani(veritabani *vt);
    void setOtobusHatDuzenle(ptrOtobusHatSinifi hat);

    void ekranaAktarma();

private slots:
    void on_btnKaydet_clicked();

    void on_btnKapat_clicked();

private:
    Ui::otobusHatDuzenle *ui;

    veritabani *su_ptrVeriTabani;
    ptrOtobusHatSinifi su_ptrOtobusHat;
};

#endif // OTOBUSHATDUZENLE_H
