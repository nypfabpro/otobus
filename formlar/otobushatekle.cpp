#include "otobushatekle.h"
#include "ui_otobushatekle.h"

otobusHatEkle::otobusHatEkle(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::otobusHatEkle)
{
    ui->setupUi(this);
    ui->lineAracKodu->setEnabled(0);
}

otobusHatEkle::~otobusHatEkle()
{
    delete ui;
}

void otobusHatEkle::setVeriTabani(veritabani *vt)
{
    if(su_ptrVeriTabani != vt) {
        su_ptrVeriTabani = vt;
    }
}

void otobusHatEkle::setOtobusHat(ptrOtobusHatSinifi hat)
{
    if(su_ptrOtobusHat != hat) {
        su_ptrOtobusHat = hat;

        ui->comboBox->addItem("");

        for(int i=0; i<su_ptrVeriTabani->OtobusDurak().count(); i++) {
            ui->comboBox->addItem(su_ptrVeriTabani->OtobusDurak().at(i)->DurakKodu());
        }

        ui->lineAracKodu->setText(su_ptrOtobusHat->AracKodu());
        ui->lineSoforAdiSoyadi->setText(su_ptrOtobusHat->SoforAdiSoyadi());
        ui->lineAyaktakiYolcuSayisi->setText(su_ptrOtobusHat->AyaktakiYolcuSayisi());
        ui->lineOturanYolcuSayisi->setText(su_ptrOtobusHat->OturanYolcuSayisi());

    }
}

void otobusHatEkle::on_btnKaydet_clicked()
{
    if( ui->lineAracKodu->text()== ""            ||
        ui->lineSoforAdiSoyadi->text()== ""     ||
        ui->lineAyaktakiYolcuSayisi->text()== ""    ||
        ui->lineOturanYolcuSayisi->text()== "") {
    QMessageBox::critical(this, "Eksik Giriş","Lütfen Gerekli Yerleri Doldurunuz.");
} else {
    su_ptrOtobusHat->setAracKodu(ui->lineAracKodu->text());
    su_ptrOtobusHat->setSoforAdiSoyadi(ui->lineSoforAdiSoyadi->text());
    su_ptrOtobusHat->setAyaktakiYolcuSayisi(ui->lineAyaktakiYolcuSayisi->text());
    su_ptrOtobusHat->setOturanYolcuSayisi(ui->lineOturanYolcuSayisi->text());

    su_ptrVeriTabani->otobusHatEkle(su_ptrOtobusHat);

    setOtobusHat(new OtobusHatSinifi);
    ui->lineAracKodu->setFocus();
   }
}

void otobusHatEkle::on_btnKapat_clicked()
{
    reject();
}
