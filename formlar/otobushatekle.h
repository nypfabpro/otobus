#ifndef OTOBUSHATEKLE_H
#define OTOBUSHATEKLE_H

#include <QDialog>
#include <QMessageBox>
#include "veriler/veritabani.h"

namespace Ui {
class otobusHatEkle;
}

class otobusHatEkle : public QDialog
{
    Q_OBJECT

public:
    explicit otobusHatEkle(QDialog *parent = 0);
    ~otobusHatEkle();

    void setVeriTabani(veritabani *vt);
    void setOtobusHat(ptrOtobusHatSinifi hat);

private slots:
    void on_btnKaydet_clicked();

    void on_btnKapat_clicked();

private:
    Ui::otobusHatEkle *ui;

    veritabani *su_ptrVeriTabani;
    ptrOtobusHatSinifi su_ptrOtobusHat;
};

#endif // OTOBUSHATEKLE_H
