#include "otobushatlar.h"
#include "ui_otobushatlar.h"

#include <QStringList>
#include <QTableWidgetItem>

otobusHatlar::otobusHatlar(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::otobusHatlar)
{
    ui->setupUi(this);
}

otobusHatlar::~otobusHatlar()
{
    delete ui;
}

void otobusHatlar::setVeriTabani(veritabani *vt)
{
    if(su_ptrVeriTabani != vt) {
        su_ptrVeriTabani = vt;

        OtobusHatSinifiListesi liste = su_ptrVeriTabani->OtobusHat();
        tabloGuncelle(liste);
    }
}

void otobusHatlar::tabloGuncelle(OtobusHatSinifiListesi liste)
{
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(liste.count());

    QStringList sutunBasliklari;
    sutunBasliklari.append("Araç Kodu");
    sutunBasliklari.append("Şoför Adı Soyadı");
    sutunBasliklari.append("Ayaktaki Yolcu Sayısı");
    sutunBasliklari.append("Oturan Yolcu Sayısı");

    ui->tableWidget->setHorizontalHeaderLabels(sutunBasliklari);

    for(int i=0;i<liste.count();i++) {

        QTableWidgetItem *kodHucresi = new QTableWidgetItem;
        kodHucresi->setText(liste[i]->AracKodu());
        ui->tableWidget->setItem(i,0,kodHucresi);

        QTableWidgetItem *adHucresi = new QTableWidgetItem;
        adHucresi->setText(liste[i]->SoforAdiSoyadi());
        ui->tableWidget->setItem(i,1,adHucresi);

        QTableWidgetItem *ayaktakiYolcuHucresi = new QTableWidgetItem;
        ayaktakiYolcuHucresi->setText(liste[i]->AyaktakiYolcuSayisi());
        ui->tableWidget->setItem(i,2,ayaktakiYolcuHucresi);

        QTableWidgetItem *oturanYolcuHucresi = new QTableWidgetItem;
        oturanYolcuHucresi->setText(liste[i]->OturanYolcuSayisi());
        ui->tableWidget->setItem(i,3,oturanYolcuHucresi);
    }
}

void otobusHatlar::on_pushButtonGuncelle_clicked()
{
    otobusHatDuzenle form;
    form.setVeriTabani(su_ptrVeriTabani);
    form.exec();

    OtobusHatSinifiListesi liste = su_ptrVeriTabani->OtobusHat();
    tabloGuncelle(liste);
}
