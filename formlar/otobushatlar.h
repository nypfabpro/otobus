#ifndef OTOBUSHATLAR_H
#define OTOBUSHATLAR_H

#include <QDialog>
#include "veriler/veritabani.h"
#include "otobushatduzenle.h"

namespace Ui {
class otobusHatlar;
}

class otobusHatlar : public QDialog
{
    Q_OBJECT

public:
    explicit otobusHatlar(QDialog *parent = 0);
    ~otobusHatlar();

    void setVeriTabani(veritabani *vt);

    void tabloGuncelle(OtobusHatSinifiListesi liste);

private slots:
    void on_pushButtonGuncelle_clicked();

private:
    Ui::otobusHatlar *ui;

    veritabani *su_ptrVeriTabani;
};

#endif // OTOBUSHATLAR_H
