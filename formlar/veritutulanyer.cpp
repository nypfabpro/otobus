#include "veritutulanyer.h"
#include "ui_veritutulanyer.h"

VeriTutulanYer::VeriTutulanYer(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::VeriTutulanYer)
{
    ui->setupUi(this);
}

VeriTutulanYer::~VeriTutulanYer()
{
    delete ui;
}

void VeriTutulanYer::setVeriTabani(veritabani *vt)
{
    if(su_ptrVeriTabani !=vt){
        su_ptrVeriTabani=vt;

        double Gelen = 0;
        if(su_ptrVeriTabani->VeriKayit().count()>0) {
            VeriKayitListesi OListe = su_ptrVeriTabani->VeriKayit();
            for(int i=0; i<OListe.count(); i++) {
                Gelen = Gelen + OListe.at(i)->OtobusGelen();
            }
            ui->lblotobus->setText(QString::number(Gelen));
        }


}
}
