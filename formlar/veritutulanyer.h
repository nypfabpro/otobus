#ifndef VERITUTULANYER_H
#define VERITUTULANYER_H

#include <QDialog>
#include "veriler/veritabani.h"

namespace Ui {
class VeriTutulanYer;
}

class VeriTutulanYer : public QDialog
{
    Q_OBJECT

public:
    explicit VeriTutulanYer(QDialog *parent = 0);
    ~VeriTutulanYer();

    void setVeriTabani(veritabani *vt);

private:
    Ui::VeriTutulanYer *ui;

    veritabani *su_ptrVeriTabani;

    double Gelen;
};

#endif // VERITUTULANYER_H
