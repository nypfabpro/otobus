#include "belediyesinifi.h"




OtobusDurakSinifi::OtobusDurakSinifi()
{

}

QString OtobusDurakSinifi::DurakAdi()
{
    return su_strdurakAdi;
}

QString OtobusDurakSinifi::DurakKodu()
{
    return su_strdurakKodu;
}

void OtobusDurakSinifi::setDurakAdi(QString yenidurakAdi)
{
    if(yenidurakAdi != su_strdurakAdi) {
        su_strdurakAdi = yenidurakAdi;
    }
}

void OtobusDurakSinifi::setDurakKodu(QString yenidurakKodu)
{
    if(yenidurakKodu != su_strdurakKodu) {
        su_strdurakKodu = yenidurakKodu;
    }
}


OtobusHatSinifi::OtobusHatSinifi()
{

}

QString OtobusHatSinifi::AracKodu()
{
    return su_strAracKodu;
}

QString OtobusHatSinifi::SoforAdiSoyadi()
{
    return su_strSofor;
}

QString OtobusHatSinifi::AyaktakiYolcuSayisi()
{
    return su_strAyaktaki;
}

QString OtobusHatSinifi::OturanYolcuSayisi()
{
    return su_strOturan;
}

void OtobusHatSinifi::setAracKodu(QString yeniAracKodu)
{
    if(yeniAracKodu != su_strAracKodu) {
        su_strAracKodu = yeniAracKodu;
    }
}

void OtobusHatSinifi::setSoforAdiSoyadi(QString yeniSoforAdiSoyadi)
{
    if(yeniSoforAdiSoyadi != su_strSofor) {
        su_strSofor = yeniSoforAdiSoyadi;
    }
}

void OtobusHatSinifi::setAyaktakiYolcuSayisi(QString yeniAyaktakiYolcuSayisi)
{
    if(yeniAyaktakiYolcuSayisi != su_strAyaktaki) {
        su_strAyaktaki = yeniAyaktakiYolcuSayisi;
    }
}

void OtobusHatSinifi::setOturanYolcuSayisi(QString yeniOturanYolcuSayisi)
{
    if(yeniOturanYolcuSayisi != su_strOturan) {
        su_strOturan = yeniOturanYolcuSayisi;
    }
}



veriKayitSinifi::veriKayitSinifi()
{    
    su_dblotobusGelen=0;    
}

double veriKayitSinifi::OtobusGelen()
{
    return su_dblotobusGelen;
}

void veriKayitSinifi::setOtobusGelen(double yeniOtobusGelen)
{
    if(yeniOtobusGelen != su_dblotobusGelen) {
        su_dblotobusGelen = yeniOtobusGelen;
    }
}

QString grafeklenti::GidilecekYer()
{
    return su_strGidilecekYer;
}


QString grafeklenti::KullanilacakHat()
{
    return su_strKullanilacakHat;
}


QString grafeklenti::KullanilacakDurak()
{
    return su_strKullanilacakDurak;
}


double grafeklenti::DurakBasinaUcret()
{
    return su_dblDurakBasinaUcret;
}


double grafeklenti::SonUcret()
{
    return su_dblSonUcret;
}


void grafeklenti::setGidilecekYer(QString yeniGidilecekYer)
{
    if(yeniGidilecekYer != su_strGidilecekYer) {
        su_strGidilecekYer = yeniGidilecekYer;
    }
}


void grafeklenti::setKullanilacakHat(QString yeniKullanilacakHat)
{
    if(yeniKullanilacakHat != su_strKullanilacakHat) {
        su_strKullanilacakHat = yeniKullanilacakHat;
    }
}


void grafeklenti::setKullanilacakDurak(QString yeniKullanilacakDurak)
{
    if(yeniKullanilacakDurak != su_strKullanilacakDurak) {
        su_strKullanilacakDurak = yeniKullanilacakDurak;
    }
}


void grafeklenti::setDurakBasinaUcret(double yeniDurakBasinaUcret)
{
    if(yeniDurakBasinaUcret != su_dblDurakBasinaUcret) {
        su_dblDurakBasinaUcret = yeniDurakBasinaUcret;
    }
}


void grafeklenti::setSonUcret(double yeniSonUcret)
{
    if(yeniSonUcret != su_dblSonUcret) {
        su_dblSonUcret = yeniSonUcret;
    }
}
