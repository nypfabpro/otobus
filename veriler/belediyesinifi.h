#ifndef BELEDIYESINIFI_H
#define BELEDIYESINIFI_H
#include <QString>


class OtobusDurakSinifi
{
public:

    OtobusDurakSinifi();

    QString DurakAdi();
    QString DurakKodu();

    void setDurakAdi(QString yenidurakAdi);
    void setDurakKodu(QString yenidurakKodu);
private:
    QString su_strdurakAdi;
    QString su_strdurakKodu;
};

class OtobusHatSinifi
{
public:
    OtobusHatSinifi();

    QString AracKodu();
    QString SoforAdiSoyadi();
    QString AyaktakiYolcuSayisi();
    QString OturanYolcuSayisi();

    void setAracKodu(QString yeniAracKodu);
    void setSoforAdiSoyadi(QString yeniSoforAdiSoyadi);
    void setAyaktakiYolcuSayisi(QString yeniAyaktakiYolcuSayisi);
    void setOturanYolcuSayisi(QString yeniOturanYolcuSayisi);
private:
    QString su_strAracKodu;
    QString su_strSofor;
    QString su_strAyaktaki;
    QString su_strOturan;
};


class  veriKayitSinifi
{
public:
    veriKayitSinifi();

    double OtobusGelen();


    void setOtobusGelen(double yeniOtobusGelen);

private:

    double su_dblotobusGelen;

};

class grafeklenti
{
public:

    QString GidilecekYer();
    QString KullanilacakHat();
    QString KullanilacakDurak();
    double DurakBasinaUcret();
    double SonUcret();

    void setGidilecekYer(QString yeniGidilecekYer);
    void setKullanilacakHat(QString yeniKullanilacakHat);
    void setKullanilacakDurak(QString yeniKullanilacakDurak);
    void setDurakBasinaUcret(double yeniDurakBasinaUcret);
    void setSonUcret(double yeniSonUcret);

private:

    QString su_strGidilecekYer;
    QString su_strKullanilacakHat;
    QString su_strKullanilacakDurak;
    double su_dblDurakBasinaUcret;
    double su_dblSonUcret;
};

/*template <class C>
class DinamikDizi {
private:
    C *u_ptrDizi;
    int u_iElemanSayisi;

public:
DinamikDizi(): u_ptrDizi(NULL), u_iElemanSayisi(0) {}

int elemanSayisi() {
    return u_iElemanSayisi;
}

C cikar(int i) {
if(u_iElemanSayisi==0) {
    throw "Hatalı İşlem";
}
int yeniElemanSayisi = u_iElemanSayisi-1;
if(yeniElemanSayisi==0) {
    C deger = u_ptrDizi[0];
    delete [] u_ptrDizi;
    u_ptrDizi = NULL;
    u_iElemanSayisi = 0;
    return deger;
}
C *yeniDizi = new C[yeniElemanSayisi];
C saglam = u_ptrDizi[i];
for(int j=0;j<yeniElemanSayisi;j++) {
    if(j!=i) {
        yeniDizi[j-(j<i?0:1)] = u_ptrDizi[j];
    }
}
delete [] u_ptrDizi;
u_iElemanSayisi = yeniElemanSayisi;
u_ptrDizi = yeniDizi;
return saglam;
}

void ekle(C e) {
    int yeniElemanSayisi = u_iElemanSayisi+1;
    C *yeniDizi = new C[yeniElemanSayisi];
    if(u_ptrDizi!=NULL) {
        C *kaynak = u_ptrDizi;
        C *hedef = yeniDizi;
        for(int i=0;i<u_iElemanSayisi;i++,hedef++,kaynak++) {
            *hedef = *kaynak;
        }
        delete [] u_ptrDizi;
    }
    u_iElemanSayisi = yeniElemanSayisi;
    u_ptrDizi = yeniDizi;
    u_ptrDizi[u_iElemanSayisi-1] = e;
}

C &eleman(int i) {
    if(i>=0 && i<u_iElemanSayisi)
        return u_ptrDizi[i];
    else
        throw "Yanlış Dizi İndisi";
}

C & operator[] (int i) {
    return eleman(i);
}

DinamikDizi<C> & operator<< (C e) {
    ekle(e);
    return *this;
}

};

class Graf {
private:
    int u_iKoseSayisi;
    DinamikDizi<int> *u_ddKomsulukListeleri;

public:
Graf(int koseSayisi) {
    u_iKoseSayisi = koseSayisi;

    u_ddKomsulukListeleri =
            new DinamikDizi<int> [u_iKoseSayisi];
}

void kenarEkle(int kaynakKose, int hedefKose) {
    u_ddKomsulukListeleri[kaynakKose].ekle(hedefKose);
    u_ddKomsulukListeleri[hedefKose].ekle(kaynakKose);
}

int enKisaYoluBul(int k, int h) {
 bool ziyaretEdildiMi[u_iKoseSayisi];
 int adimSayisi[u_iKoseSayisi];

 for(int i=0;i<u_iKoseSayisi;i++) {
     ziyaretEdildiMi[i] = false;
     adimSayisi[i] = numeric_limits<int>::max();
 }

 ziyaretEdildiMi[k] = true;
 adimSayisi[k] = 0;

 while(true) {

for(int i=0;
    i<u_ddKomsulukListeleri[k].elemanSayisi();
    i++) {
    int hedef = u_ddKomsulukListeleri[k][i];
    int yeniAdimSayisi = adimSayisi[k]+1;
    if(yeniAdimSayisi<adimSayisi[hedef]) {
        adimSayisi[hedef] = yeniAdimSayisi;
    }
}

int geciciEnKucukAdimSayisi = numeric_limits<int>::max();
bool kaynakBulunduMu = false;

for(int i=0;i<u_iKoseSayisi;i++) {
    if(ziyaretEdildiMi[i]==false) {
        if(adimSayisi[i]<geciciEnKucukAdimSayisi) {
            geciciEnKucukAdimSayisi = adimSayisi[i];
            k=i;
            kaynakBulunduMu = true;
        }
    }
}
if(k==h) {
    return adimSayisi[h];
}
if(kaynakBulunduMu==false) {
    return -1;
}
ziyaretEdildiMi[k] = true;
 }
}

};
*/
#endif // BELEDIYESINIFI_H
