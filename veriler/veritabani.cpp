#include "veritabani.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>


veritabani::veritabani()
{
}

OtobusDurakSinifiListesi veritabani::OtobusDurak()
{
    return su_lstOtobusDurak;
}

OtobusHatSinifiListesi veritabani::OtobusHat()
{
    return su_lstOtobusHat;
}



VeriKayitListesi veritabani::VeriKayit()
{
    return su_lstVeriKayit;
}

GrafEklentiListesi veritabani::GrafEklenti()
{
    return su_lstGrafEklenti;
}


void veritabani::otobusDurakEkle(ptrOtobusDurakSinifi durak)
{
    su_lstOtobusDurak.append(durak);
}

void veritabani::otobusHatEkle(ptrOtobusHatSinifi hat)
{
    su_lstOtobusHat.append(hat);
}


void veritabani::veriKayitEkle(ptrVeriKayit veri)
{
    su_lstVeriKayit.append(veri);
}

void veritabani::grafEkle(ptrGrafEklenti graf)
{
    su_lstGrafEklenti.append(graf);
}

void veritabani::bilgileriKaydet()
{
    if(QSqlDatabase::database("Veritabani").open()) {
    QSqlQuery sorgu(QSqlDatabase::database("Veritabani"));



        sorgu.prepare("CREATE TABLE IF NOT EXISTS OtobusDurakSinifi ("
                      "durakadi text," "durakkodu text)");
        if(!sorgu.exec()) {
            return;
        }

        sorgu.prepare("DELETE FROM OtobusDurakSinifi");
        if(!sorgu.exec()) {
            return;
    }
        sorgu.prepare("INSERT INTO OtobusDurakSinifi "
                      "VALUES (:durakadi, :durakkodu)");

        foreach(ptrOtobusDurakSinifi oto, su_lstOtobusDurak) {
            sorgu.bindValue(":durakadi", oto->DurakAdi());
            sorgu.bindValue(":durakkodu", oto->DurakKodu());

            sorgu.exec();
        }

        sorgu.prepare("CREATE TABLE IF NOT EXISTS OtobusHatSinifi ("
                      "arackodu text," "soforadisoyadi text," "ayaktakiyolcu text," "oturanyolcu text)");
        if(!sorgu.exec()) {
            return;
        }

        sorgu.prepare("DELETE FROM OtobusHatSinifi");
        if(!sorgu.exec()) {
            return;
    }
        sorgu.prepare("INSERT INTO OtobusHatSinifi "
                      "VALUES (:arackodu, :soforadisoyadi," ":ayaktakiyolcu," ":oturanyolcu)");

        foreach(ptrOtobusHatSinifi oto, su_lstOtobusHat) {
            sorgu.bindValue(":arackodu", oto->AracKodu());
            sorgu.bindValue(":soforadisoyadi", oto->SoforAdiSoyadi());
            sorgu.bindValue(":ayaktakiyolcu", oto->AyaktakiYolcuSayisi());
            sorgu.bindValue(":oturanyolcu", oto->OturanYolcuSayisi());

            sorgu.exec();
        }        
        sorgu.prepare("DELETE FROM veriKayitSinifi");
        if(!sorgu.exec()) {
            return;
    }
        sorgu.prepare("INSERT INTO veriKayitSinifi "
                      "VALUES (:otobusgelen, :metrobusgelen, :tramvaygelen, :metrogelen, :denizaracigelen)");

        foreach(ptrVeriKayit veri, su_lstVeriKayit) {
            sorgu.bindValue(":otobusgelen", veri->OtobusGelen());            

            sorgu.exec();
        }

        sorgu.prepare("CREATE TABLE IF NOT EXISTS grafeklenti ("
                      "yer text," "hat text,"
                      "durak text," "durakucret double,"
                      "sonucret double)");
        if(!sorgu.exec()) {
            return;
        }

        sorgu.prepare("DELETE FROM grafeklenti");
        if(!sorgu.exec()) {
            return;
    }
        sorgu.prepare("INSERT INTO grafeklenti "
                      "VALUES (:yer, :hat, :durak, :durakucret, :sonucret)");

        foreach(ptrGrafEklenti graf, su_lstGrafEklenti) {
            sorgu.bindValue(":yer", graf->GidilecekYer());
            sorgu.bindValue(":hat", graf->KullanilacakHat());
            sorgu.bindValue(":durak", graf->KullanilacakDurak());
            sorgu.bindValue(":durakucret", graf->DurakBasinaUcret());
            sorgu.bindValue(":sonucret", graf->SonUcret());

            sorgu.exec();
        }
        QSqlDatabase::database("Veritabani").close();
        QSqlDatabase::removeDatabase("Veritabani");
    }
}
void veritabani::bilgileriOku()
{
    QSqlDatabase::addDatabase("QSQLITE","Veritabani");
    QSqlDatabase::database("Veritabani").setDatabaseName("c:\\qt\\belediye.vt");
    if(QSqlDatabase::database("Veritabani").open()) {

        QSqlQuery sorgu(QSqlDatabase::database("Veritabani"));




        while(sorgu.next()) {
            ptrOtobusDurakSinifi oto = new OtobusDurakSinifi;

            oto->setDurakAdi(sorgu.value("durakadi").toString());
            oto->setDurakKodu(sorgu.value("durakkodu").toString());

            otobusDurakEkle(oto);
        }

        sorgu.prepare("SELECT * FROM OtobusHatSinifi");

        if(!sorgu.exec()) {
            return;
        }

        while(sorgu.next()) {
            ptrOtobusHatSinifi oto = new OtobusHatSinifi;

            oto->setAracKodu(sorgu.value("arackodu").toString());
            oto->setSoforAdiSoyadi(sorgu.value("soforadisoyadi").toString());
            oto->setAyaktakiYolcuSayisi(sorgu.value("ayaktakiyolcu").toString());
            oto->setOturanYolcuSayisi(sorgu.value("oturanyolcu").toString());

            otobusHatEkle(oto);
        }

        sorgu.prepare("SELECT * FROM MetrobusDurakSinifi");

        if(!sorgu.exec()) {
            return;
        }



        while(sorgu.next()) {
            ptrVeriKayit veri = new veriKayitSinifi;

            veri->setOtobusGelen(sorgu.value("otobusgelen").toDouble());            

            veriKayitEkle(veri);
        }

        sorgu.prepare("SELECT * FROM grafeklenti");

        if(!sorgu.exec()) {
            return;
        }

        while(sorgu.next()) {
            ptrGrafEklenti graf = new grafeklenti;

            graf->setGidilecekYer(sorgu.value("otobusgelen").toString());
            graf->setKullanilacakHat(sorgu.value("metrobusgelen").toString());
            graf->setKullanilacakDurak(sorgu.value("tramvaygelen").toString());
            graf->setDurakBasinaUcret(sorgu.value("metrogelen").toDouble());
            graf->setSonUcret(sorgu.value("denizaracigelen").toDouble());

            grafEkle(graf);
        }
    }
}
