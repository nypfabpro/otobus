#ifndef VERITABANI_H
#define VERITABANI_H

#include "veritabani.h"
#include "belediyesinifi.h"

#include <QList>
#include <QMap>
#include <QDate>
#include <QFile>


typedef OtobusDurakSinifi* ptrOtobusDurakSinifi;
typedef OtobusHatSinifi* ptrOtobusHatSinifi;

typedef veriKayitSinifi* ptrVeriKayit;
typedef grafeklenti* ptrGrafEklenti;


typedef QList<ptrOtobusDurakSinifi> OtobusDurakSinifiListesi;
typedef QList<ptrOtobusHatSinifi> OtobusHatSinifiListesi;

typedef QList<ptrVeriKayit> VeriKayitListesi;
typedef QList<ptrGrafEklenti> GrafEklentiListesi;


class veritabani
{
public:
    veritabani();


    OtobusDurakSinifiListesi OtobusDurak();
    OtobusHatSinifiListesi OtobusHat();

    VeriKayitListesi VeriKayit();
    GrafEklentiListesi GrafEklenti();



    void otobusDurakEkle(ptrOtobusDurakSinifi durak);
    void otobusHatEkle(ptrOtobusHatSinifi hat);

    void veriKayitEkle(ptrVeriKayit veri);
    void grafEkle(ptrGrafEklenti graf);

    void bilgileriKaydet();
    void bilgileriOku();


private:

    OtobusDurakSinifiListesi su_lstOtobusDurak;
    OtobusHatSinifiListesi su_lstOtobusHat;

    VeriKayitListesi su_lstVeriKayit;
    GrafEklentiListesi su_lstGrafEklenti;
};

#endif // VERITABANI_H
